package controllers

import (
	"errors"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"log"
	"net/http"
)

// CreateReaction godoc
// @Summary Create a reaction
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param post body models.Reaction true "type_id and parent_id are required fields"
// @Success 200 {object} models.Reaction
// @Router /reaction [post]
func (s *Server) CreateReaction(w http.ResponseWriter, r *http.Request) {
	m := models.NewReaction()
	s.create(m, w, r)
}

// ReadAllReactionByPost godoc
// @Summary Get all reactions for post
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param post_id path string true "UUID"
// @Success 200 {array} models.Reaction
// @Router /post/{post_id}/reaction [get]
func (s *Server) ReadAllReactionByPost(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	m := models.NewReaction()
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByParentId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, all)
}

// ReadAllReactionByComment godoc
// @Summary Get all reactions for post comment
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param comment_id path string true "UUID"
// @Success 200 {array} models.Reaction
// @Router /post/comment/{comment_id}/reaction [get]
func (s *Server) ReadAllReactionByComment(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	m := models.NewReaction()
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByParentId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, all)
}

//func (s *Server) ReadReaction(w http.ResponseWriter, r *http.Request) {
//	m := models.NewReaction()
//	s.read(m, w, r)
//}

//func (s *Server) UpdateReaction(w http.ResponseWriter, r *http.Request) {
//	m := models.NewReaction()
//	s.update(m, w, r)
//}

// DeleteReaction godoc
// @Summary Delete reaction
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param reaction_id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /reaction/{reaction_id} [delete]
func (s *Server) DeleteReaction(w http.ResponseWriter, r *http.Request) {
	m := models.NewReaction()
	s.delete(m, w, r)
}

// CreateReactionType godoc
// @Summary Create a reaction type
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param post body models.ReactionType true "name is required field"
// @Success 200 {object} models.ReactionType
// @Router /type/reaction [post]
func (s *Server) CreateReactionType(w http.ResponseWriter, r *http.Request) {
	if auth.CheckTokenRole(auth.RoleAdmin, w, r) {
		m := models.ReactionType{}
		s.create(&m, w, r)
	} else {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
	}
}

// ReadReactionType godoc
// @Summary Read reaction type
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param type_id path string true "UUID"
// @Success 200 {object} models.ReactionType
// @Router /type/reaction/{type_id} [get]
func (s *Server) ReadReactionType(w http.ResponseWriter, r *http.Request) {
	m := models.ReactionType{}
	s.read(&m, w, r)
}

// ReadAllReactionTypes godoc
// @Summary Read all reaction types
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Success 200 {array} models.ReactionType
// @Router /type/reaction [get]
func (s *Server) ReadAllReactionTypes(w http.ResponseWriter, r *http.Request) {
	m := models.ReactionType{}
	s.readAll(&m, w, r)
}

// UpdateReactionType godoc
// @Summary Update reaction type
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param type_id path string true "UUID"
// @Success 200 {object} models.ReactionType
// @Router /type/reaction/{type_id} [put]
func (s *Server) UpdateReactionType(w http.ResponseWriter, r *http.Request) {
	if auth.CheckTokenRole(auth.RoleAdmin, w, r) {
		m := models.ReactionType{}
		s.update(&m, w, r)
	} else {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
	}
}

// DeleteReactionType godoc
// @Summary Delete reaction type
// @Tags Reaction
// @Accept  json
// @Produce  json
// @Param type_id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /type/reaction/{type_id} [delete]
func (s *Server) DeleteReactionType(w http.ResponseWriter, r *http.Request) {
	if auth.CheckTokenRole(auth.RoleAdmin, w, r) {
		rm := models.NewReaction()
		vars := mux.Vars(r)
		id, err := uuid.Parse(vars["id"])
		if err != nil {
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}
		all, err := rm.GetByTypeId(s.DB, id)
		if err != nil {
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}
		if len(all) == 0 {
			responses.ERROR(w, http.StatusBadRequest, errors.New("reaction with this type exist"))
			return
		}

		tm := models.ReactionType{}
		s.delete(&tm, w, r)
	} else {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
	}
}

func (s *Server) deleteReactionByPost(db *gorm.DB, id, uId uuid.UUID) error {
	r := models.NewReaction()
	a, err := r.GetByParentId(db, id)
	if err != nil {
		log.Println("deleteReactionByPost: ", err)
		return err
	}
	for _, v := range a {
		err = s.pushDelMsg(v)
		if err != nil {
			log.Println("deleteReactionByPost: ", err)
			return err
		}
		err = v.Delete(db)
		if err != nil {
			log.Println("deleteReactionByPost: ", err)
			return err
		}
	}
	return nil
}

func (s *Server) deleteReactionByPostComment(db *gorm.DB, id, uId uuid.UUID) error {
	r := models.NewReaction()
	a, err := r.GetByParentId(db, id)
	if err != nil {
		log.Println("deleteReactionByPostComment: ", err)
		return err
	}
	for _, v := range a {
		err = s.pushDelMsg(v)
		if err != nil {
			log.Println("deleteReactionByPostComment: ", err)
			return err
		}
		err = v.Delete(db)
		if err != nil {
			log.Println("deleteReactionByPostComment: ", err)
			return err
		}
	}
	return nil
}
