package controllers

import (
	"errors"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"net/http"
	"os"
)

// ReadAllDeleteAttach godoc
// @Summary Get all attach meta-objects marked to be deleted
// @Tags Attach
// @Accept  json
// @Produce  json
// @Success 200 {array} models.DeletedAttach
// @Router /deleted/attach [get]
func (s *Server) ReadAllDeleteAttach(w http.ResponseWriter, r *http.Request) {
	m := models.DeletedAttach{}
	s.readAll(&m, w, r)
}

// ReadDeleteAttach godoc
// @Summary Get attach meta-object marked to be deleted
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {array} models.DeletedAttach
// @Router /deleted/attach/{id} [get]
func (s *Server) ReadDeleteAttach(w http.ResponseWriter, r *http.Request) {
	m := models.DeletedAttach{}
	s.read(&m, w, r)
}

// DeleteDeleteAttach godoc
// @Summary Delete attach meta-object marked to be deleted and file
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /deleted/attach/{id} [delete]
func (s *Server) DeleteDeleteAttach(w http.ResponseWriter, r *http.Request) {
	if auth.CheckTokenRole(auth.RoleAdmin, w, r) {
		vars := mux.Vars(r)
		id, err := uuid.Parse(vars["id"])
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		code, err := s.deleteAttachment(id)
		if err != nil {
			responses.ERROR(w, code, err)
		} else {
			responses.JSON(w, code, nil)
		}
	} else {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
	}
}

func (s *Server) deleteAttachment(id uuid.UUID) (int, error) {
	m := models.NewDeleteAttach()
	err := m.GetById(s.DB, id)
	if err != nil {
		return http.StatusNotFound, err
	}
	db := s.DB.Begin()
	err = db.Exec("delete from files where id = ?", m.Id).Error
	if err != nil {
		return http.StatusInternalServerError, err
	}

	err = os.Remove(m.Path)

	//	nPath := filepath.Join(s.DelPath + "/" + m.Name)
	//	err = os.Rename(m.Path, nPath)
	if err != nil {
		db.Rollback()
		return http.StatusNotFound, err
	}
	//err = s.pushDelMsg(m)
	//if err != nil {
	//	db.Rollback()
	//	return http.StatusServiceUnavailable, err
	//}
	db.Commit()

	return http.StatusOK, nil
}
