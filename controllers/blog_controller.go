package controllers

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"gitlab.com/bigboagolang/blog/sql"
	"gopkg.in/resty.v1"
	"io/ioutil"
	"log"
	"net/http"
)

// CreateBlog godoc
// @Summary Create a blog
// @Tags Blog
// @Accept  json
// @Produce  json
// @Param blog body models.Blog true "group_id and name are required fields"
// @Success 200 {object} models.Blog
// @Router /blog [post]
func (s *Server) CreateBlog(w http.ResponseWriter, r *http.Request) {

	//	_ = struct {name string; group_id string}{}
	m := models.NewBlog()
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = json.Unmarshal(body, &m)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	f, err := s.checkGroup(m.GroupId, r.Header.Get("Authorization"))
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	if !f {
		responses.ERROR(w, http.StatusNotFound, errors.New("group not found"))
		return
	}

	m.Prepare(aToken.AccountId)
	err = m.Verify(s.DB, "")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	db := s.DB.Begin()
	err = m.Create(db)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	err = s.pushPostMsg(m)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	db.Commit()
	responses.JSON(w, http.StatusCreated, m)
}

// ReadBlog godoc
// @Summary Get a blog
// @Tags Blog
// @Accept  json
// @Produce  json
// @Param blog_id path int true "UUID"
// @Success 200 {object} models.Blog
// @Router /blog/{blog_id} [get]
func (s *Server) ReadBlog(w http.ResponseWriter, r *http.Request) {
	m := models.Blog{}
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	res, err := s.getTags(m.TagIds, r.Header.Get("Authorization"))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	m.Tags = res

	responses.JSON(w, http.StatusOK, m)
}

// ReadAllBlogs godoc
// @Summary Get all blogs
// @Tags Blog
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Blog
// @Router /blog [get]
func (s *Server) ReadAllBlogs(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	m := models.Blog{}
	all, err := m.GetAll(s.DB)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	blogs := make([]models.Blog, 0)
	for _, v := range all.([]models.Blog) {
		res, err := s.getTags(v.TagIds, r.Header.Get("Authorization"))
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		v.Tags = res
		blogs = append(blogs, v)
	}

	responses.JSON(w, http.StatusOK, blogs)
}

// UpdateBlog godoc
// @Summary Update a blog
// @Tags Blog
// @Accept  json
// @Produce  json
// @Param blog_id path int true "UUID"
// @Success 200 {array} models.Blog
// @Router /blog/{blog_id} [put]
func (s *Server) UpdateBlog(w http.ResponseWriter, r *http.Request) {
	m := models.NewBlog()
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	if !auth.CheckTokenRole(auth.RoleAdmin, w, r) || !auth.CanUpdateBlog(s.DB, id, aToken.AccountId) {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = json.Unmarshal(body, &m)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	f, err := s.checkGroup(m.GroupId, r.Header.Get("Authorization"))
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	if !f {
		responses.ERROR(w, http.StatusNotFound, errors.New("group not found"))
		return
	}

	err = m.Verify(s.DB, vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	db := s.DB.Begin()
	err = m.Update(db, aToken.AccountId)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	err = s.pushPutMsg(m)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	db.Commit()
	responses.JSON(w, http.StatusOK, m)
}

// DeleteBlog godoc
// @Summary Delete a blog
// @Tags Blog
// @Accept  json
// @Produce  json
// @Param blog_id path int true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /blog/{blog_id} [delete]
func (s *Server) DeleteBlog(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	if !auth.CheckTokenRole(auth.RoleAdmin, w, r) || !auth.CanDeleteBlog(s.DB, id, aToken.AccountId) {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
		return
	}

	blog := models.NewBlog()
	db := s.DB.Begin()
	err = s.deleteAttachByBlog(db, id, aToken.AccountId)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	err = s.deletePostByBlog(db, id, aToken.AccountId)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	if !s.delete(blog, w, r) {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	db.Commit()
}

func (s *Server) deleteAttachByBlog(db *gorm.DB, bId, uId uuid.UUID) error {
	a := make([]models.Attach, 0)
	err := db.Raw(sql.GetAllPostsAttachByBlogId, bId).Find(&a).Error
	if err != nil {
		log.Println("GetAllPostsAttachByBlogId error: ", err.Error())
		return err
	}
	for _, v := range a {
		v.DeletedBy = uId
		err = v.Delete(db)
		if err != nil {
			log.Println("Delete error: ", err.Error())
			return err
		}
		err = s.pushDelMsg(v)
		if err != nil {
			return err
		}
	}
	err = db.Raw(sql.GetAllPostCommentsAttachByBlogId, bId).Find(&a).Error
	if err != nil {
		log.Println("GetAllPostCommentsAttachByBlogId error: ", err.Error())
		return err
	}
	for _, v := range a {
		v.DeletedBy = uId
		err = v.Delete(db)
		if err != nil {
			log.Println("Delete error: ", err.Error())
			return err
		}
		err = s.pushDelMsg(v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Server) deletePostByBlog(db *gorm.DB, bId, uId uuid.UUID) error {
	m := models.NewPost()
	p, err := m.GetByBlogId(db, bId)
	if err != nil {
		log.Println("deletePostByBlog: ", err.Error())
		return err
	}
	for _, v := range p {
		err = s.pushDelMsg(v)
		if err != nil {
			log.Println("deletePostByBlog: ", err.Error())
			return err
		}
		err = s.deletePostCommentByPost(db, v.Id, uId)
		if err != nil {
			log.Println("deletePostByBlog: ", err.Error())
			return err
		}
		err = s.deleteReactionByPost(db, v.Id, uId)
		if err != nil {
			log.Println("deletePostByBlog: ", err.Error())
			return err
		}
		err = v.Delete(db)
		if err != nil {
			log.Println("deletePostByBlog: ", err.Error())
			return err
		}
	}
	return nil
}

// TagBlog godoc
// @Summary Tag a blog
// @Tags Blog
// @Accept  json
// @Produce  json
// @Param blog_id path int true "UUID"
// @Param tags body models.Tag true "json array of strings"
// @Success 200 {array} models.Blog
// @Router /blog/{blog_id}/tags [post]
func (s *Server) TagBlog(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	c := resty.New()
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", r.Header.Get("Authorization")).
		SetBody(body).
		Post(s.TagServiceUrl)

	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	res := make([]models.Tag, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	m := models.NewBlog()
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	tags := ""
	for _, v := range res {
		if len(tags) > 0 {
			tags += ","
		}
		tags += v.Id.String()
	}
	if len(tags) > 0 {
		err = m.Tag(s.DB, tags)
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
	}
	res, err = s.getTags(m.TagIds, r.Header.Get("Authorization"))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	m.Tags = res
	err = s.pushPutMsg(m)
	if err != nil {
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	responses.JSON(w, http.StatusOK, m)
}
