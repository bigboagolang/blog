package controllers

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"io/ioutil"
	"net/http"
)

// CreateUserRole godoc
// @Summary Create a user role
// @Tags BlogAdmin
// @Accept  json
// @Produce  json
// @Param post body models.BlogAdmin true "user_id and blog_id are required fields"
// @Success 200 {object} models.BlogAdmin
// @Router /role [post]
func (s *Server) CreateUserRole(w http.ResponseWriter, r *http.Request) {
	m := models.BlogAdmin{}
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = json.Unmarshal(body, &m)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	f, err := s.checkUser(m.UserId, r.Header.Get("Authorization"))
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	if !f {
		responses.ERROR(w, http.StatusNotFound, errors.New("user not not found"))
		return
	}

	m.Prepare(aToken.AccountId)
	err = m.Verify(s.DB, "")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = m.Create(s.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	//w.Header().Set("Lacation", fmt.Sprintf("%s%s/%s", r.Host, r.URL.Path, ""))
	responses.JSON(w, http.StatusCreated, m)
}

//func (s *Server) ReadUserRole(w http.ResponseWriter, r *http.Request) {
//	m := models.BlogAdmin{}
//	s.read(&m, w, r)
//}

// ReadAllUserRole godoc
// @Summary Get all user roles
// @Tags BlogAdmin
// @Accept  json
// @Produce  json
// @Success 200 {array} models.BlogAdmin
// @Router /role [get]
func (s *Server) ReadAllUserRole(w http.ResponseWriter, r *http.Request) {
	m := models.BlogAdmin{}
	s.readAll(&m, w, r)
}

// ReadUserRoleByBlogId godoc
// @Summary Get all user roles for blog
// @Tags BlogAdmin
// @Accept  json
// @Produce  json
// @Param blog_id path string true "UUID"
// @Success 200 {array} models.BlogAdmin
// @Router /blog/{blog_id}/role [get]
func (s *Server) ReadUserRoleByBlogId(w http.ResponseWriter, r *http.Request) {

	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	m := models.BlogAdmin{}
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByBlogId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, all)
}

// ReadUserRoleByUserId godoc
// @Summary Get all user roles for user
// @Tags BlogAdmin
// @Accept  json
// @Produce  json
// @Param blog_id path string true "UUID"
// @Success 200 {array} models.BlogAdmin
// @Router /user/{id}/role [get]
func (s *Server) ReadUserRoleByUserId(w http.ResponseWriter, r *http.Request) {

	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	m := models.BlogAdmin{}
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByUserId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, all)
}

// DeleteUserRole godoc
// @Summary Delete user role
// @Tags BlogAdmin
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /role/{id} [delete]
func (s *Server) DeleteUserRole(w http.ResponseWriter, r *http.Request) {
	m := models.BlogAdmin{}
	s.delete(&m, w, r)
}
