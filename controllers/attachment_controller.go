package controllers

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"io/ioutil"
	"mime"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

// CreateAttach godoc
// @Summary Create a attachment
// @Tags Attach
// @Accept  mpfd
// @Produce  json
// @Param post body models.Attach true "parent_id and uploadFile are required fields"
// @Success 200 {object} models.Attach
// @Router /attach [post]
func (s *Server) CreateAttach(w http.ResponseWriter, r *http.Request) {
	pId := r.PostFormValue("parent_id")
	if len(pId) == 0 {
		responses.ERROR(w, http.StatusBadRequest, errors.New("parent_id is missing"))
		return
	}
	// validate file size
	r.Body = http.MaxBytesReader(w, r.Body, s.MaxUploadSize)
	if err := r.ParseMultipartForm(s.MaxUploadSize); err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	// parse and validate file and post parameters
	file, handler, err := r.FormFile("uploadFile")
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	defer file.Close()
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	detectedFileType := http.DetectContentType(fileBytes)
	aType := models.AttachType{}
	err = aType.GetByType(s.DB, detectedFileType)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, errors.New("INVALID_FILE_TYPE"))
		return
	}

	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}
	a := models.NewAttach()
	a.OrigName = handler.Filename
	ParentId, err := uuid.Parse(r.PostFormValue("parent_id"))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, errors.New("parent_id error: "+err.Error()))
		return
	}
	a.ParentId = ParentId

	a.CreatedBy = uuid.Nil
	c := r.PostFormValue("created_by")
	if len(c) > 0 {
		CreatedBy, err := uuid.Parse(c)
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		a.CreatedBy = CreatedBy
	}

	a.Prepare(aToken.AccountId)
	fileName := a.Id.String()
	fileEndings, err := mime.ExtensionsByType(detectedFileType)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	dir := s.UploadPath + "/" + a.ParentId.String()
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		os.Mkdir(dir, os.ModeDir)
	}
	a.Name = fileName + fileEndings[0]
	newPath := filepath.Join(dir, a.Name)
	fmt.Printf("FileType: %s, File: %s\n", detectedFileType, newPath)

	// write file

	newFile, err := os.Create(newPath)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	defer newFile.Close() // idempotent, okay to call twice
	if _, err := newFile.Write(fileBytes); err != nil || newFile.Close() != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	a.Path = newPath
	a.Url = "/files/" + a.ParentId.String() + "/" + a.Name

	db := s.DB.Begin()
	err = a.Create(db)
	if err != nil {
		_ = os.Remove(newFile.Name())
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	err = s.pushPostMsg(a)
	if err != nil {
		db.Rollback()
		_ = os.Remove(newFile.Name())
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	db.Commit()
	responses.JSON(w, http.StatusCreated, a)
}

// ReadAttach godoc
// @Summary Read attach meta-object
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {object} models.Attach
// @Router /attach/{id} [get]
func (s *Server) ReadAttach(w http.ResponseWriter, r *http.Request) {
	m := models.Attach{}
	s.read(&m, w, r)
}

// ReadAttachFile godoc
// @Summary Read attach file
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param path_to_file path string true "path to the file on the server"
// @Success 200 {string} string
// @Router /files/{path_to_file} [get]
func (s *Server) ReadAttachFile(prefix string, h http.Handler) http.Handler {
	if prefix == "" {
		return h
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		aToken := auth.GetAccessToken(w, r)
		if aToken == nil {
			return
		}
		if p := strings.TrimPrefix(r.URL.Path, prefix); len(p) < len(r.URL.Path) {
			r2 := new(http.Request)
			*r2 = *r
			r2.URL = new(url.URL)
			*r2.URL = *r.URL
			r2.URL.Path = p
			h.ServeHTTP(w, r2)
		} else {
			http.NotFound(w, r)
		}
	})
}

// DeleteAttach godoc
// @Summary Mark attach meta-object to be deleted
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /attach/{id} [delete]
func (s *Server) DeleteAttach(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	m := models.NewAttach()
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.JSON(w, http.StatusNotFound, err)
		return
	}

	m.DeletedBy = aToken.AccountId
	db := s.DB.Begin()
	nPath := filepath.Join(s.DelPath + "/" + m.Name)
	err = os.Rename(m.Path, nPath)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	m.Path = nPath
	err = m.Delete(db)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	err = s.pushDelMsg(m)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	db.Commit()

	responses.JSON(w, http.StatusOK, nil)
}

// CreateAttachType godoc
// @Summary Create a attachment type
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param post body models.AttachType true "type is required field"
// @Success 200 {object} models.AttachType
// @Router /type/attach [post]
func (s *Server) CreateAttachType(w http.ResponseWriter, r *http.Request) {
	if auth.CheckTokenRole(auth.RoleAdmin, w, r) {
		m := models.AttachType{}
		s.create(&m, w, r)
	} else {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
	}
}

// ReadAttachType godoc
// @Summary Read attachment type
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {object} models.AttachType
// @Router /type/attach/{id} [get]
func (s *Server) ReadAttachType(w http.ResponseWriter, r *http.Request) {
	m := models.AttachType{}
	s.read(&m, w, r)
}

// ReadAllAttachTypes godoc
// @Summary Read all attachment type
// @Tags Attach
// @Accept  json
// @Produce  json
// @Success 200 {array} models.AttachType
// @Router /type/attach [get]
func (s *Server) ReadAllAttachTypes(w http.ResponseWriter, r *http.Request) {
	m := models.AttachType{}
	s.readAll(&m, w, r)
}

// UpdateAttachType godoc
// @Summary Update attachment type
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {object} models.AttachType
// @Router /type/attach/{id} [put]
func (s *Server) UpdateAttachType(w http.ResponseWriter, r *http.Request) {
	if auth.CheckTokenRole(auth.RoleAdmin, w, r) {
		m := models.AttachType{}
		s.update(&m, w, r)
	} else {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
	}
}

// DeleteAttachType godoc
// @Summary Delete attachment type
// @Tags Attach
// @Accept  json
// @Produce  json
// @Param id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /type/attach/{id} [delete]
func (s *Server) DeleteAttachType(w http.ResponseWriter, r *http.Request) {
	if auth.CheckTokenRole(auth.RoleAdmin, w, r) {
		m := models.AttachType{}
		s.delete(&m, w, r)
	} else {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
	}
}
