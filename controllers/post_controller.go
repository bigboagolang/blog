package controllers

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"gitlab.com/bigboagolang/blog/sql"
	"gopkg.in/resty.v1"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// CreatePost godoc
// @Summary Create a post
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post body models.Post true "blog_id and title are required fields"
// @Success 200 {object} models.Post
// @Router /post [post]
func (s *Server) CreatePost(w http.ResponseWriter, r *http.Request) {
	m := models.NewPost()
	s.create(m, w, r)
}

// ReadPost godoc
// @Summary Get a post
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post_id path string true "UUID"
// @Success 200 {object} models.Post
// @Router /post/{post_id} [get]
func (s *Server) ReadPost(w http.ResponseWriter, r *http.Request) {
	m := models.NewPost()
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	res, err := s.getTags(m.TagIds, r.Header.Get("Authorization"))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	m.Tags = res

	responses.JSON(w, http.StatusOK, m)
}

// ReadAllPosts godoc
// @Summary Get all posts from the blog
// @Tags Post
// @Accept  json
// @Produce  json
// @Param blog_id path string true "UUID"
// @Success 200 {array} models.Post
// @Router /blog/{blog_id}/post [get]
func (s *Server) ReadAllPosts(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	m := models.NewPost()
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByBlogId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	posts := make([]models.Post, 0)
	for _, v := range all {
		res, err := s.getTags(v.TagIds, r.Header.Get("Authorization"))
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		v.Tags = res
		posts = append(posts, v)
	}
	responses.JSON(w, http.StatusOK, posts)
}

// ReadPostAttachments godoc
// @Summary Get all attachments for the post
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post_id path string true "UUID"
// @Success 200 {array} models.Attach
// @Router /post/{post_id}/attach [get]
func (s *Server) ReadPostAttachments(w http.ResponseWriter, r *http.Request) {
	m := models.NewAttach()
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByParentId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	responses.JSON(w, http.StatusOK, all)
}

// ReadPostByUser godoc
// @Summary Get all posts created by user
// @Tags Post
// @Accept  json
// @Produce  json
// @Param user_id path string true "UUID"
// @Success 200 {array} models.Attach
// @Router /user/{user_id}/post [get]
func (s *Server) ReadPostByUser(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	m := models.NewPost()
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByUserId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	posts := make([]models.Post, 0)
	for _, v := range all {
		res, err := s.getTags(v.TagIds, r.Header.Get("Authorization"))
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
		v.Tags = res
		posts = append(posts, v)
	}
	responses.JSON(w, http.StatusOK, posts)
}

// UpdatePost godoc
// @Summary Update post
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post_id path string true "UUID"
// @Success 200 {object} models.Attach
// @Router /post/{post_id} [put]
func (s *Server) UpdatePost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}
	if !auth.CheckTokenRole(auth.RoleAdmin, w, r) || !auth.CanUpdatePost(s.DB, id, aToken.AccountId) {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
		return
	}
	m := models.NewPost()
	s.update(m, w, r)
}

// DeletePost godoc
// @Summary Delete post
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post_id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /post/{post_id} [delete]
func (s *Server) DeletePost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}
	owner, mod := auth.CanDeletePost(s.DB, id, aToken.AccountId)
	if !auth.CheckTokenRole(auth.RoleAdmin, w, r) || !owner {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
		return
	}
	post := models.NewPost()
	if mod {
		err = post.GetById(s.DB, id)
		if err != nil {
			responses.ERROR(w, http.StatusNotFound, err)
			return
		}
		post.ToRemoveWhen = time.Now()
		post.ToRemove = true
		post.ToRemoveBy = aToken.AccountId
		err := s.DB.Model(&models.Post{}).Save(post).Error
		if err != nil {
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}
		return
	}

	db := s.DB.Begin()
	err = s.deleteAttachByPost(db, id, aToken.AccountId)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	err = s.deleteReactionByPost(db, id, aToken.AccountId)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	err = s.deletePostCommentByPost(db, post.Id, aToken.AccountId)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	if !s.delete(post, w, r) {
		db.Rollback()
		return
	}
	db.Commit()
}

func (s *Server) deleteAttachByPost(db *gorm.DB, bId, uId uuid.UUID) error {
	a := make([]models.Attach, 0)
	err := db.Raw(sql.GetAllAttachByParentId, bId).Find(&a).Error
	if err != nil {
		log.Println("GetAllAttachByParentId error: ", err.Error())
		return err
	}
	for _, v := range a {
		v.DeletedBy = uId
		err = v.Delete(db)
		if err != nil {
			log.Println("Delete error: ", err.Error())
			return err
		}
		err = s.pushDelMsg(v)
		if err != nil {
			db.Rollback()
			return err
		}
	}
	err = db.Raw(sql.GetAllPostCommentsAttachByPostId, bId).Find(&a).Error
	if err != nil {
		log.Println("GetAllPostCommentsAttachByPostId error: ", err.Error())
		return err
	}
	for _, v := range a {
		v.DeletedBy = uId
		err = v.Delete(db)
		if err != nil {
			log.Println("Delete error: ", err.Error())
			return err
		}
		err = s.pushDelMsg(v)
		if err != nil {
			db.Rollback()
			return err
		}
	}
	return nil
}

func (s *Server) deletePostCommentByPost(db *gorm.DB, pId, uId uuid.UUID) error {
	m := models.NewPostComment()
	pc, err := m.GetByPostId(db, pId)
	if err != nil {
		log.Println("deleteCommentByPost: ", err.Error())
		return err
	}
	for _, v := range pc {
		err = s.deleteReactionByPostComment(db, v.Id, uId)
		if err != nil {
			log.Println("deleteCommentByPost: ", err.Error())
			return err
		}
		err = s.pushDelMsg(v)
		if err != nil {
			log.Println("deleteCommentByPost: ", err.Error())
			return err
		}
		err = v.Delete(db)
		if err != nil {
			log.Println("deleteCommentByPost: ", err.Error())
			return err
		}
	}
	return nil
}

// TagPost godoc
// @Summary Tag a post
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post_id path int true "UUID"
// @Param tags body models.Tag true "json array of strings"
// @Success 200 {array} models.Tag
// @Router /post/{post_id}/tags [post]
func (s *Server) TagPost(w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	c := resty.New()
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", r.Header.Get("Authorization")).
		SetBody(body).
		Post(s.TagServiceUrl)

	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	res := make([]models.Tag, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	m := models.NewPost()
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	tags := ""
	for _, v := range res {
		if len(tags) > 0 {
			tags += ","
		}
		tags += v.Id.String()
	}
	if len(tags) > 0 {
		err = m.Tag(s.DB, tags)
		if err != nil {
			responses.ERROR(w, http.StatusBadRequest, err)
			return
		}
	}
	res, err = s.getTags(m.TagIds, r.Header.Get("Authorization"))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	m.Tags = res
	err = s.pushPutMsg(m)
	if err != nil {
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	responses.JSON(w, http.StatusOK, m)
}

// CreatePostComment godoc
// @Summary Create a post comment
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post body models.PostComment true "post_id and comment are required fields"
// @Success 200 {object} models.PostComment
// @Router /post/comment [post]
func (s *Server) CreatePostComment(w http.ResponseWriter, r *http.Request) {
	m := models.NewPostComment()
	s.create(m, w, r)
}

// ReadPostComment godoc
// @Summary Get a post comment
// @Tags Post
// @Accept  json
// @Produce  json
// @Param comment_id path string true "UUID"
// @Success 200 {object} models.PostComment
// @Router /post/comment/{comment_id} [get]
func (s *Server) ReadPostComment(w http.ResponseWriter, r *http.Request) {
	m := models.NewPostComment()
	s.read(m, w, r)
}

// ReadPostCommentAttachments godoc
// @Summary Get all attachments for the post comment
// @Tags Post
// @Accept  json
// @Produce  json
// @Param comment_id path string true "UUID"
// @Success 200 {array} models.Attach
// @Router /post/comment/{comment_id}/attach [get]
func (s *Server) ReadPostCommentAttachments(w http.ResponseWriter, r *http.Request) {
	m := models.NewAttach()
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByParentId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	responses.JSON(w, http.StatusOK, all)
}

// ReadPostComments godoc
// @Summary Get all comments for the post
// @Tags Post
// @Accept  json
// @Produce  json
// @Param post_id path string true "UUID"
// @Success 200 {array} models.PostComment
// @Router /post/{post_id}/comment [get]
func (s *Server) ReadPostComments(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	m := models.NewPostComment()
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	all, err := m.GetByPostId(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, all)
}

// UpdatePostComment godoc
// @Summary Update post comment
// @Tags Post
// @Accept  json
// @Produce  json
// @Param comment_id path string true "UUID"
// @Success 200 {object} models.PostComment
// @Router /post/comment/{comment_id} [put]
func (s *Server) UpdatePostComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}
	if !auth.CheckTokenRole(auth.RoleAdmin, w, r) || !auth.CanUpdatePostComment(s.DB, id, aToken.AccountId) {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
		return
	}

	m := models.NewPostComment()
	s.update(m, w, r)
}

// DeletePostComment godoc
// @Summary Delete post comment
// @Tags Post
// @Accept  json
// @Produce  json
// @Param comment_id path string true "UUID"
// @Success 200 {string} string http.StatusOK
// @Router /post/comment/{comment_id} [delete]
func (s *Server) DeletePostComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}
	owner, mod := auth.CanDeletePostComment(s.DB, id, aToken.AccountId)
	if !auth.CheckTokenRole(auth.RoleAdmin, w, r) || !owner {
		responses.ERROR(w, http.StatusForbidden, errors.New("operation is not allowed"))
		return
	}
	post := models.NewPostComment()
	if mod {
		err = post.GetById(s.DB, id)
		if err != nil {
			responses.ERROR(w, http.StatusNotFound, err)
			return
		}
		post.ToRemoveWhen = time.Now()
		post.ToRemove = true
		post.ToRemoveBy = aToken.AccountId
		err := s.DB.Model(&models.PostComment{}).Save(post).Error
		if err != nil {
			responses.ERROR(w, http.StatusInternalServerError, err)
			return
		}
		return
	}
	db := s.DB.Begin()
	err = s.deleteAttachByPostComment(db, id, aToken.AccountId)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	if !s.delete(post, w, r) {
		db.Rollback()
	}
	db.Commit()

	//m := models.NewPostComment()
	//s.delete(m, w, r)
}

func (s *Server) deleteAttachByPostComment(db *gorm.DB, bId, uId uuid.UUID) error {
	a := make([]models.Attach, 0)
	err := db.Raw(sql.GetAllAttachByParentId, bId).Find(&a).Error
	if err != nil {
		log.Println("GetAllPostsAttachByBlogId error: ", err.Error())
		return err
	}
	for _, v := range a {
		v.DeletedBy = uId
		err = v.Delete(db)
		if err != nil {
			log.Println("Delete error: ", err.Error())
			return err
		}
		err = s.pushDelMsg(v)
		if err != nil {
			db.Rollback()
			return err
		}
	}
	return nil
}
