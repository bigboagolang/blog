package controllers

import (
	"encoding/json"
	"errors"
	"github.com/streadway/amqp"
	"gitlab.com/bigboagolang/blog/models"
)

type RabbitMQMessage struct {
	Index  string      `json:"index"`
	Type   string      `json:"type"`
	Object interface{} `json:"object"`
}

func (s *Server) pushPostMsg(d interface{}) error {
	return s.pushMsg(d, "elasticsearchPost")
}

func (s *Server) pushPutMsg(d interface{}) error {
	return s.pushMsg(d, "elasticsearchPut")
}

func (s *Server) pushDelMsg(d interface{}) error {
	return s.pushMsg(d, "elasticsearchDelete")
}

func (s *Server) pushMsg(d interface{}, key string) error {
	msg := s.insertType(d)

	url := "amqp://" + s.RabbitUserName + ":" + s.RabbitPassword + "@" + s.RabbitHost + ":" + s.RabbitPort
	conn, err := amqp.Dial(url)
	if err != nil {
		return errors.New("failed to connect to RabbitMQ: " + err.Error())
	}
	defer conn.Close()
	ch, err := conn.Channel()

	if err != nil {
		return errors.New("failed to open a channel: " + err.Error())
	}
	defer ch.Close()

	//	body := {"index":"university-index-v1","type":"_doc","object":{"id":"c76a21be-604e-40d5-971b-273c4ca8deab"}}
	body, err := json.Marshal(msg)

	if err != nil {
		return err
	}
	err = ch.Publish(
		"",    // exchange
		key,   // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(string(body)),
		})

	return nil
}

func (s *Server) insertType(d interface{}) interface{} {
	var buff interface{}

	switch d.(type) {
	case *models.Blog:
		m := d.(*models.Blog)
		buff = struct {
			*models.Blog
			Type string `json:"type"`
		}{
			Blog: m,
			Type: "blog",
		}
	case *models.Post:
		m := d.(*models.Post)
		buff = struct {
			*models.Post
			Type string `json:"type"`
		}{
			Post: m,
			Type: "post",
		}
	case models.Post:
		m := d.(models.Post)
		buff = struct {
			models.Post
			Type string `json:"type"`
		}{
			Post: m,
			Type: "post",
		}
	case *models.PostComment:
		m := d.(*models.PostComment)
		buff = struct {
			*models.PostComment
			Type string `json:"type"`
		}{
			PostComment: m,
			Type:        "post_comment",
		}
	case models.PostComment:
		m := d.(models.PostComment)
		buff = struct {
			models.PostComment
			Type string `json:"type"`
		}{
			PostComment: m,
			Type:        "post_comment",
		}

	case *models.Attach:
		m := d.(*models.Attach)
		buff = struct {
			*models.Attach
			Type string `json:"type"`
		}{
			Attach: m,
			Type:   "attach",
		}
	case models.Attach:
		m := d.(models.Attach)
		buff = struct {
			models.Attach
			Type string `json:"type"`
		}{
			Attach: m,
			Type:   "attach",
		}
	case *models.DeletedAttach:
		m := d.(*models.DeletedAttach)
		buff = struct {
			*models.DeletedAttach
			Type string `json:"type"`
		}{
			DeletedAttach: m,
			Type:          "attach",
		}
	case *models.Reaction:
		m := d.(*models.Reaction)
		buff = struct {
			*models.Reaction
			Type string `json:"type"`
		}{
			Reaction: m,
			Type:     "reaction",
		}
	case models.Reaction:
		m := d.(models.Reaction)
		buff = struct {
			models.Reaction
			Type string `json:"type"`
		}{
			Reaction: m,
			Type:     "reaction",
		}
	}

	msg := struct {
		Index  string      `json:"index"`
		Type   string      `json:"type"`
		Object interface{} `json:"object"`
	}{
		Index:  s.Index,
		Type:   s.Type,
		Object: buff,
	}
	return msg
}
