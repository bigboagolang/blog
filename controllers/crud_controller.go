package controllers

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"io/ioutil"
	"net/http"
)

func (s *Server) create(m models.Model, w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = json.Unmarshal(body, &m)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	m.Prepare(aToken.AccountId)
	err = m.Verify(s.DB, "")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	db := s.DB.Begin()
	err = m.Create(db)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	//w.Header().Set("Lacation", fmt.Sprintf("%s%s/%s", r.Host, r.URL.Path, ""))
	err = s.pushPostMsg(m)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	db.Commit()
	responses.JSON(w, http.StatusCreated, m)
}

func (s *Server) read(m models.Model, w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, m)
}

func (s *Server) readAll(m models.Model, w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	all, err := m.GetAll(s.DB)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	responses.JSON(w, http.StatusOK, all)
}

func (s *Server) update(m models.Model, w http.ResponseWriter, r *http.Request) {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return
	}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = json.Unmarshal(body, &m)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	err = m.Verify(s.DB, vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	db := s.DB.Begin()
	err = m.Update(db, aToken.AccountId)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	err = s.pushPutMsg(m)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return
	}
	db.Commit()
	responses.JSON(w, http.StatusOK, m)
}

func (s *Server) delete(m models.Model, w http.ResponseWriter, r *http.Request) bool {
	aToken := auth.GetAccessToken(w, r)
	if aToken == nil {
		return false
	}

	//if !aToken.CheckTokenRole(auth.RoleAdmin) {
	//	responses.ERROR(w, http.StatusForbidden, errors.New("only Admin user can delete blog"))
	//	return false
	//}

	vars := mux.Vars(r)
	id, err := uuid.Parse(vars["id"])
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return false
	}
	err = m.GetById(s.DB, id)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return false
	}
	db := s.DB.Begin()
	err = m.Delete(db)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return false
	}
	err = s.pushDelMsg(m)
	if err != nil {
		db.Rollback()
		responses.ERROR(w, http.StatusServiceUnavailable, err)
		return false
	}
	db.Commit()

	//w.WriteHeader(http.StatusOK)
	responses.JSON(w, http.StatusOK, nil)
	return true
}

//func (s *Server) deleteWithAttach(m models.Model, w http.ResponseWriter, r *http.Request) bool {
//	aToken := auth.GetAccessToken(w, r)
//	if aToken == nil {
//		return false
//	}
//
//	vars := mux.Vars(r)
//	id, err := uuid.Parse(vars["id"])
//	if err != nil {
//		responses.ERROR(w, http.StatusBadRequest, err)
//		return false
//	}
//	err = m.GetById(s.DB, id)
//	if err != nil {
//		responses.ERROR(w, http.StatusNotFound, err)
//		return false
//	}
//	db := s.DB.Begin()
//	err = m.Delete(db)
//	if err != nil {
//		responses.ERROR(w, http.StatusInternalServerError, err)
//		return false
//	}
//	a := models.NewAttach()
//	allAttach, err := a.GetByParentId(db, id)
//	if err != nil {
//		responses.ERROR(w, http.StatusInternalServerError, err)
//		return false
//	}
//	for _, v := range allAttach {
//		code, err := s.deleteAttachment(v.Id)
//		if err != nil {
//			db.Rollback()
//			responses.ERROR(w, code, err)
//			return false
//		}
//	}
//	err = s.pushDelMsg(m)
//	if err != nil {
//		db.Rollback()
//		responses.ERROR(w, http.StatusServiceUnavailable, err)
//		return false
//	}
//	db.Commit()
//
//	//w.WriteHeader(http.StatusOK)
//	responses.JSON(w, http.StatusOK, nil)
//	return true
//}
