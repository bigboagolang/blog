package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gitlab.com/bigboagolang/blog/auth"
	"gitlab.com/bigboagolang/blog/models"
	"gopkg.in/resty.v1"
	"log"
	"net/http"
	"strings"
)

type Server struct {
	DB     *gorm.DB
	Router *mux.Router

	DbHost     string
	DbDriver   string
	DbUser     string
	DbPassword string
	DbName     string
	DbPort     string

	ClientID  string
	ConfigURL string

	MaxUploadSize int64
	UploadPath    string
	DelPath       string

	TagServiceUrl  string
	UserServiceUrl string

	RabbitHost     string
	RabbitPort     string
	RabbitUserName string
	RabbitPassword string
	Index          string
	Type           string

	LogFilename   string
	LogMaxSize    int
	LogMaxBackups int
	LogMaxAge     int
	LogCompress   bool
}

var err error

func (s *Server) Run(addr string) {
	log.Println("Listening to port 8080")
	log.Fatal(http.ListenAndServe(addr, s.Router))
}

func (s *Server) Initialize() {
	DbUrl := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", s.DbHost, s.DbPort, s.DbUser, s.DbName, s.DbPassword)
	s.DB, err = gorm.Open(s.DbDriver, DbUrl)
	if err != nil {
		fmt.Printf("Cannot connect to %s database\n", s.DbDriver)
		log.Fatal("This is the error:", err)
	} else {
		fmt.Printf("We are connected to the %s database\n", s.DbDriver)
	}

	auth.InitAuthConf(s.ClientID, s.ConfigURL)

	s.initDB()

	s.Router = mux.NewRouter()

	s.initializeRoutes()

	//log.SetOutput(&lumberjack.Logger{
	//	Filename:   s.LogFilename,
	//	MaxSize:    s.LogMaxSize,
	//	MaxBackups: s.LogMaxBackups,
	//	MaxAge:     s.LogMaxAge,
	//	Compress:   s.LogCompress,
	//})
}

func (s *Server) initDB() {
	s.createTable(models.ReactionType{})
	s.createTable(models.Reaction{})
	s.createTable(models.Post{})
	s.createTable(models.Blog{})
	s.createTable(models.BlogAdmin{})
	s.createTable(models.PostComment{})
	s.createTable(models.Attach{})
	s.createTable(models.AttachType{})

	//if s.DB.Model(models.Post{}).AddForeignKey("blog_id", "blogs(id)", "CASCADE", "NO ACTION").Error != nil {
	//	log.Fatal(err)
	//}
	//if s.DB.Model(models.PostComment{}).AddForeignKey("post_id", "posts(id)", "CASCADE", "NO ACTION").Error != nil {
	//	log.Fatal(err)
	//}
	if s.DB.Model(models.BlogAdmin{}).AddForeignKey("blog_id", "blogs(id)", "CASCADE", "NO ACTION").Error != nil {
		log.Fatal(err)
	}
	//if s.DB.Model(models.Reaction{}).AddForeignKey("type_id", "reaction_types(id)", "CASCADE", "NO ACTION").Error != nil {
	//	log.Fatal(err)
	//}
	//if s.DB.Model(models.Reaction{}).AddForeignKey("parent_id", "posts(id)", "CASCADE", "NO ACTION").Error != nil {
	//	log.Fatal(err)
	//}
	//if s.DB.Model(models.Reaction{}).AddForeignKey("parent_id", "blogs(id)", "CASCADE", "NO ACTION").Error != nil {
	//	log.Fatal(err)
	//}
}

func (s *Server) createTable(v interface{}) {
	if !s.DB.HasTable(v) {
		err = s.DB.CreateTable(v).Error
		if err != nil {
			log.Fatal("Error while creating table:", err)
		}
	}
}

func (s *Server) getTags(tags, token string) ([]models.Tag, error) {
	ids := strings.Split(tags, ",")
	i := len(ids)
	body := "["
	for k, v := range ids {
		body += "\"" + v + "\""
		if k < i-1 {
			body += ", "
		}
	}
	body += "]"
	c := resty.New()
	resp, err := c.R().
		SetHeader("Content-Type", "application/json").
		SetHeader("Authorization", token).
		SetBody(body).
		Post(s.TagServiceUrl + "/searchById")
		//Post("http://172.28.128.45:9112/api/v1/tags/searchById")

	if err != nil {
		return nil, err
	}

	res := make([]models.Tag, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *Server) checkUser(id uuid.UUID, token string) (bool, error) {
	c := resty.New()
	resp, err := c.R().
		SetHeader("Authorization", token).
		SetPathParams(map[string]string{"accountId": id.String()}).
		Get(s.UserServiceUrl + "/user/{accountId}")

	if err != nil {
		return false, err
	}
	if resp.StatusCode() != 200 {
		return false, nil
	}
	return true, nil
}

func (s *Server) checkGroup(id uuid.UUID, token string) (bool, error) {
	c := resty.New()
	resp, err := c.R().
		SetHeader("Authorization", token).
		SetPathParams(map[string]string{"gId": id.String()}).
		Get(s.UserServiceUrl + "/group/{gId}")

	if err != nil {
		return false, err
	}
	if resp.StatusCode() != 200 {
		return false, nil
	}
	return true, nil
}
