package controllers

import (
	"github.com/swaggo/http-swagger"
	_ "gitlab.com/bigboagolang/blog/docs"
	"net/http"
)

// @title Swagger Blog API
// @version 1.2
// @host localhost:8080
// @BasePath

func (s *Server) initializeRoutes() {
	s.Router.HandleFunc("/blog", s.CreateBlog).Methods("POST")
	s.Router.HandleFunc("/blog", s.ReadAllBlogs).Methods("GET")
	s.Router.HandleFunc("/blog/{id}", s.UpdateBlog).Methods("PUT")
	s.Router.HandleFunc("/blog/{id}", s.ReadBlog).Methods("GET")
	s.Router.HandleFunc("/blog/{id}", s.DeleteBlog).Methods("DELETE")
	s.Router.HandleFunc("/blog/{id}/tags", s.TagBlog).Methods("POST")
	s.Router.HandleFunc("/blog/{id}/post", s.ReadAllPosts).Methods("GET")

	s.Router.HandleFunc("/post", s.CreatePost).Methods("POST")
	s.Router.HandleFunc("/post/{id}", s.UpdatePost).Methods("PUT")
	s.Router.HandleFunc("/post/{id}", s.ReadPost).Methods("GET")
	s.Router.HandleFunc("/post/{id}", s.DeletePost).Methods("DELETE")
	s.Router.HandleFunc("/post/{id}/tags", s.TagPost).Methods("POST")
	s.Router.HandleFunc("/post/{id}/comment", s.ReadPostComments).Methods("GET")
	s.Router.HandleFunc("/post/{id}/attach", s.ReadPostAttachments).Methods("GET")

	s.Router.HandleFunc("/post/comment", s.CreatePostComment).Methods("POST")
	s.Router.HandleFunc("/post/comment/{id}", s.ReadPostComment).Methods("GET")
	s.Router.HandleFunc("/post/comment/{id}", s.UpdatePostComment).Methods("PUT")
	s.Router.HandleFunc("/post/comment/{id}", s.DeletePostComment).Methods("DELETE")
	s.Router.HandleFunc("/post/comment/{id}/attach", s.ReadPostCommentAttachments).Methods("GET")

	s.Router.HandleFunc("/user/{id}/post", s.ReadPostByUser).Methods("GET")

	s.Router.HandleFunc("/attach", s.CreateAttach).Methods("POST")
	s.Router.HandleFunc("/attach/{id}", s.ReadAttach).Methods("GET")
	s.Router.HandleFunc("/attach/{id}", s.DeleteAttach).Methods("DELETE")
	fs := http.FileServer(http.Dir(s.UploadPath))
	s.Router.PathPrefix("/files/").Handler(s.ReadAttachFile("/files", fs)).Methods("GET")

	s.Router.HandleFunc("/deleted/attach", s.ReadAllDeleteAttach).Methods("GET")
	s.Router.HandleFunc("/deleted/attach/{id}", s.ReadDeleteAttach).Methods("GET")
	s.Router.HandleFunc("/deleted/attach/{id}", s.DeleteDeleteAttach).Methods("DELETE")

	s.Router.HandleFunc("/type/attach", s.CreateAttachType).Methods("POST")
	s.Router.HandleFunc("/type/attach", s.ReadAllAttachTypes).Methods("GET")
	s.Router.HandleFunc("/type/attach/{id}", s.UpdateAttachType).Methods("PUT")
	s.Router.HandleFunc("/type/attach/{id}", s.ReadAttachType).Methods("GET")
	s.Router.HandleFunc("/type/attach/{id}", s.DeleteAttachType).Methods("DELETE")

	s.Router.HandleFunc("/reaction", s.CreateReaction).Methods("POST")
	//	s.Router.HandleFunc("/reaction/{id}", s.UpdateReaction).Methods("PUT")
	s.Router.HandleFunc("/post/{id}/reaction", s.ReadAllReactionByPost).Methods("GET")
	s.Router.HandleFunc("/post/comment/{id}/reaction", s.ReadAllReactionByComment).Methods("GET")
	s.Router.HandleFunc("/reaction/{id}", s.DeleteReaction).Methods("DELETE")

	s.Router.HandleFunc("/type/reaction", s.CreateReactionType).Methods("POST")
	s.Router.HandleFunc("/type/reaction", s.ReadAllReactionTypes).Methods("GET")
	s.Router.HandleFunc("/type/reaction/{id}", s.UpdateReactionType).Methods("PUT")
	s.Router.HandleFunc("/type/reaction/{id}", s.ReadReactionType).Methods("GET")
	s.Router.HandleFunc("/type/reaction/{id}", s.DeleteReactionType).Methods("DELETE")

	s.Router.HandleFunc("/role", s.CreateUserRole).Methods("POST")
	s.Router.HandleFunc("/role", s.ReadAllUserRole).Methods("GET")
	s.Router.HandleFunc("/user/{id}/role", s.ReadUserRoleByUserId).Methods("GET")
	s.Router.HandleFunc("/blog/{id}/role", s.ReadUserRoleByBlogId).Methods("GET")
	s.Router.HandleFunc("/role/{id}", s.DeleteUserRole).Methods("DELETE")

	//http://localhost:8080/swagger/index.html
	s.Router.PathPrefix("/swagger/").Handler(httpSwagger.Handler(
		httpSwagger.URL("/swagger/doc.json"),
	))

}
