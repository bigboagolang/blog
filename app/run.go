package app

import (
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres database driver
	"github.com/joho/godotenv"
	"gitlab.com/bigboagolang/blog/controllers"
	"log"
	"os"
	"strconv"
)

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Println("sad .env file found")
	}
}

func Run() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	} else {
		log.Println("We are getting the env values")
	}

	srvUrl := os.Getenv("SERVER_URL")
	MaxUploadSize, err := strconv.ParseInt(os.Getenv("MAX_UPLOAD_SIZE"), 10, 64)
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	}
	LogCompress, err := strconv.ParseBool(os.Getenv("LOG_FILE_COMPRESS"))
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	}
	LogMaxSize, err := strconv.Atoi(os.Getenv("LOG_FILE_MAX_SIZE_MB"))
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	}
	LogMaxBackups, err := strconv.Atoi(os.Getenv("LOG_FILE_MAX_BACKUPS"))
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	}
	LogMaxAge, err := strconv.Atoi(os.Getenv("LOG_FILE_MAX_AGE_DAYS"))
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	}
	server := controllers.Server{
		DbDriver:   os.Getenv("DB_DRIVER"),
		DbHost:     os.Getenv("DB_HOST"),
		DbName:     os.Getenv("DB_NAME"),
		DbUser:     os.Getenv("DB_USER"),
		DbPassword: os.Getenv("DB_PASSWORD"),
		DbPort:     os.Getenv("DB_PORT"),

		ClientID:      os.Getenv("CLIENT_ID"),
		ConfigURL:     os.Getenv("AUTH_URL"),
		MaxUploadSize: MaxUploadSize,
		UploadPath:    os.Getenv("FILES_PATH"),
		DelPath:       os.Getenv("DEL_FILES_PATH"),

		TagServiceUrl:  os.Getenv("TAG_SERVICE_URL"),
		UserServiceUrl: os.Getenv("USER_SERVICE_URL"),
		RabbitHost:     os.Getenv("RABBIT_HOST"),
		RabbitPort:     os.Getenv("RABBIT_PORT"),
		RabbitUserName: os.Getenv("RABBIT_USERNAME"),
		RabbitPassword: os.Getenv("RABBIT_PASSWORD"),
		Index:          os.Getenv("ELASTICSEARCH_INDEX"),
		Type:           os.Getenv("ELASTICSEARCH_TYPE"),

		LogFilename:   os.Getenv("LOG_FILE_NAME"),
		LogCompress:   LogCompress,
		LogMaxSize:    LogMaxSize,
		LogMaxBackups: LogMaxBackups,
		LogMaxAge:     LogMaxAge,
	}
	server.Initialize()
	server.Run(srvUrl)
}
