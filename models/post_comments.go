package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"time"
)

type PostComment struct {
	Id           uuid.UUID `json:"id" gorm:"column:id"`
	PostId       uuid.UUID `json:"post_id" gorm:"column:post_id"`
	ParentID     uuid.UUID `json:"parent_id" gorm:"column:parent_id"`
	Comment      string    `json:"comment" gorm:"column:comment"`
	ToRemove     bool      `json:"to_remove" gorm:"column:to_remove"`
	ToRemoveBy   uuid.UUID `json:"remove_by" gorm:"column:remove_by"`
	ToRemoveWhen time.Time `json:"remove_when" gorm:"column:remove_when"`
	CreatedBy    uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen  time.Time `json:"created_when" gorm:"column:created_when"`
	Updated      bool      `json:"updated" gorm:"column:updated"`
	UpdatedBy    uuid.UUID `json:"updated_by,omitempty" gorm:"column:updated_by"`
	UpdatedWhen  time.Time `json:"updated_when,omitempty" gorm:"column:updated_when"`
}

func (r *PostComment) TableName() string {
	return "post_comments"
}

func NewPostComment() *PostComment {
	return &PostComment{}
}

func (r *PostComment) Create(db *gorm.DB) error {
	err := db.Debug().Model(&PostComment{}).Create(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *PostComment) GetAll(db *gorm.DB) (interface{}, error) {
	return nil, nil
}

func (r *PostComment) GetByPostId(db *gorm.DB, id uuid.UUID) ([]PostComment, error) {
	all := make([]PostComment, 0)
	err := db.Debug().Model(&PostComment{}).Where("post_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (r *PostComment) GetByParentId(db *gorm.DB, id uuid.UUID) (interface{}, error) {
	all := make([]PostComment, 5)
	err := db.Debug().Model(&PostComment{}).Where("parent_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (r *PostComment) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&PostComment{}).Where("id = ?", id).Take(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *PostComment) Update(db *gorm.DB, id uuid.UUID) error {
	r.Updated = true
	r.UpdatedWhen = time.Now()
	r.UpdatedBy = id
	err := db.Debug().Model(&PostComment{}).Save(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *PostComment) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&PostComment{}).Where("id = ?", r.Id).Take(&PostComment{}).Delete(&PostComment{}).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("post comment not found")
		}
		return err
	}
	return nil
}

func (r *PostComment) Verify(db *gorm.DB, id string) error {
	if r.Id == uuid.Nil {
		return errors.New("id is empty")
	}
	if r.PostId == uuid.Nil {
		return errors.New("post_id is empty")
	} else {
		p := Post{
			Id: uuid.Nil,
		}
		err := p.GetById(db, r.PostId)
		if err != nil || p.Id == uuid.Nil {
			return errors.New("post_id not found")
		}
	}
	if r.ParentID != uuid.Nil {
		p := PostComment{
			Id: uuid.Nil,
		}
		err := p.GetById(db, r.ParentID)
		if err != nil || p.Id == uuid.Nil {
			return errors.New("parent_id not found")
		}
	}
	if r.Comment == "" {
		return errors.New("comment is empty")
	}
	return nil
}

func (r *PostComment) Prepare(aId uuid.UUID) {
	r.Id = uuid.New()
	r.CreatedWhen = time.Now()
	if r.CreatedBy == uuid.Nil {
		r.CreatedBy = aId
	}
}
