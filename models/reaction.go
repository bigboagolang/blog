package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"time"
)

type Reaction struct {
	Id          uuid.UUID `json:"id" gorm:"column:id"`
	TypeId      uuid.UUID `json:"type_id" gorm:"column:type_id"`
	ParentId    uuid.UUID `json:"parent_id" gorm:"column:parent_id"`
	CreatedBy   uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen time.Time `json:"created_when" gorm:"column:created_when"`
	//Updated     bool      `gorm:"-"`
	//UpdatedBy   uuid.UUID `gorm:"-"`
	//UpdatedWhen time.Time `gorm:"-"`
}

func (r *Reaction) TableName() string {
	return "reactions"
}

func NewReaction() *Reaction {
	return &Reaction{}
}

func (r *Reaction) Create(db *gorm.DB) error {
	err := db.Debug().Model(&Reaction{}).Create(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *Reaction) GetAll(db *gorm.DB) (interface{}, error) {
	return nil, nil
}

func (r *Reaction) GetById(db *gorm.DB, id uuid.UUID) error {
	//err := db.Raw(sql.GetById, id).Scan(r).Error

	err := db.Debug().Model(&Reaction{}).Where("id = ?", id).Take(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *Reaction) GetByParentId(db *gorm.DB, id uuid.UUID) ([]Reaction, error) {
	all := make([]Reaction, 0)
	err := db.Debug().Model(&Reaction{}).Where("parent_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (r *Reaction) GetByTypeId(db *gorm.DB, id uuid.UUID) ([]Reaction, error) {
	all := make([]Reaction, 0)
	err := db.Debug().Model(&Reaction{}).Where("type_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (r *Reaction) Update(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&Reaction{}).Save(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *Reaction) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&Reaction{}).Where("id = ?", r.Id).Take(&Reaction{}).Delete(&Reaction{}).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("reaction not found")
		}
		return err
	}
	return nil
}

func (r *Reaction) Verify(db *gorm.DB, id string) error {
	if r.Id == uuid.Nil {
		return errors.New("id is empty")
	}
	if r.TypeId == uuid.Nil {
		return errors.New("type is empty")
	}
	if r.ParentId == uuid.Nil {
		return errors.New("parent_id is empty")
	}
	if id != "" && r.Id != uuid.Nil && r.Id.String() != id {
		return errors.New("id in URL is not equal to id in body")
	}
	return nil
}

func (r *Reaction) Prepare(aId uuid.UUID) {
	r.Id = uuid.New()
	r.CreatedWhen = time.Now()

	if r.CreatedBy == uuid.Nil {
		r.CreatedBy = aId
	}
}
