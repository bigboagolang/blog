package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"time"
)

type BlogAdmin struct {
	Id          uuid.UUID `json:"id" gorm:"column:id"`
	BlogId      uuid.UUID `json:"blog_id" gorm:"column:blog_id"`
	UserId      uuid.UUID `json:"user_id" gorm:"column:user_id"`
	CreatedBy   uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen time.Time `json:"created_when" gorm:"column:created_when"`
	//Updated     bool      `gorm:"-"`
	//UpdatedBy   uuid.UUID `gorm:"-"`
	//UpdatedWhen time.Time `gorm:"-"`
}

func (u *BlogAdmin) TableName() string {
	return "blogs_admins"
}

func (u *BlogAdmin) Create(db *gorm.DB) error {
	err := db.Debug().Model(&BlogAdmin{}).Create(u).Error
	if err != nil {
		return err
	}
	return nil
}

func (u *BlogAdmin) GetAll(db *gorm.DB) (interface{}, error) {
	all := make([]BlogAdmin, 5)
	err := db.Debug().Model(&BlogAdmin{}).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (u *BlogAdmin) GetByBlogId(db *gorm.DB, id uuid.UUID) (interface{}, error) {
	all := make([]BlogAdmin, 5)
	err := db.Debug().Model(&BlogAdmin{}).Where("blog_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (u *BlogAdmin) GetByUserId(db *gorm.DB, id uuid.UUID) ([]BlogAdmin, error) {
	all := make([]BlogAdmin, 0)
	err := db.Debug().Model(&BlogAdmin{}).Where("user_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (u *BlogAdmin) GetByUserAndBlogId(db *gorm.DB, uId uuid.UUID, bId uuid.UUID) ([]BlogAdmin, error) {
	all := make([]BlogAdmin, 0)
	err := db.Debug().Model(&BlogAdmin{}).Where("user_id = ? and blog_id = ?", uId, bId).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (u *BlogAdmin) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&BlogAdmin{}).Where("id = ?", id).Take(u).Error
	if err != nil {
		return err
	}
	return nil
}

func (u *BlogAdmin) Update(db *gorm.DB, id uuid.UUID) error {
	return nil
}

func (u *BlogAdmin) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&BlogAdmin{}).Where("id = ?", u.Id).Take(&BlogAdmin{}).Delete(&BlogAdmin{}).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("role not found")
		}
		return err
	}
	return nil
}

func (u *BlogAdmin) Verify(db *gorm.DB, id string) error {
	if u.Id == uuid.Nil {
		return errors.New("id is empty")
	}
	if u.BlogId == uuid.Nil {
		return errors.New("blog_id is empty")
	} else if id != "" {
		m := Blog{
			Id: uuid.Nil,
		}
		bId, err := uuid.Parse(id)
		if err != nil {
			return err
		}
		err = m.GetById(db, bId)
		if err != nil || m.Id == uuid.Nil {
			return errors.New("blog_id not found")
		}
	}
	if u.UserId == uuid.Nil {
		return errors.New("user_id is empty")
	}
	if id != "" && u.Id != uuid.Nil && u.Id.String() != id {
		return errors.New("id in URL is not equal to id in body")
	}
	return nil
}

func (u *BlogAdmin) Prepare(aId uuid.UUID) {
	u.Id = uuid.New()
	u.CreatedWhen = time.Now()
	if u.CreatedBy == uuid.Nil {
		u.CreatedBy = aId
	}
}
