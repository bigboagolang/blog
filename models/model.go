package models

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type Model interface {
	Prepare(aId uuid.UUID)
	Verify(db *gorm.DB, id string) error
	Create(db *gorm.DB) error
	Update(db *gorm.DB, id uuid.UUID) error
	Delete(db *gorm.DB) error
	GetById(db *gorm.DB, id uuid.UUID) error
	GetAll(db *gorm.DB) (interface{}, error)
}
