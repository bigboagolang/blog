package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"html"
	"strings"
	"time"
)

type ReactionType struct {
	Id          uuid.UUID `json:"id" gorm:"column:id"`
	Name        string    `json:"name" gorm:"column:name"`
	CreatedBy   uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen time.Time `json:"created_when" gorm:"column:created_when"`
	Updated     bool      `json:"updated" gorm:"column:updated"`
	UpdatedBy   uuid.UUID `json:"updated_by,omitempty" gorm:"column:updated_by"`
	UpdatedWhen time.Time `json:"updated_when,omitempty" gorm:"column:updated_when"`
}

func (r *ReactionType) TableName() string {
	return "reaction_types"
}

func (r *ReactionType) Create(db *gorm.DB) error {
	err := db.Debug().Model(&ReactionType{}).Create(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *ReactionType) GetAll(db *gorm.DB) (interface{}, error) {
	all := make([]ReactionType, 5)
	err := db.Debug().Model(&ReactionType{}).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (r *ReactionType) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&ReactionType{}).Where("id = ?", id).Take(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *ReactionType) Update(db *gorm.DB, id uuid.UUID) error {
	r.Updated = true
	r.UpdatedWhen = time.Now()
	r.UpdatedBy = id
	err := db.Debug().Model(&ReactionType{}).Save(r).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *ReactionType) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&ReactionType{}).Where("id = ?", r.Id).Take(&ReactionType{}).Delete(&ReactionType{}).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("reaction type not found")
		}
		return err
	}
	return nil
}

func (r *ReactionType) Verify(db *gorm.DB, id string) error {
	if r.Id == uuid.Nil {
		return errors.New("id is empty")
	}
	if r.Name == "" {
		return errors.New("name is empty")
	}

	return nil
}

func (r *ReactionType) Prepare(aId uuid.UUID) {
	r.Id = uuid.New()
	r.Name = html.EscapeString(strings.TrimSpace(r.Name))
	r.CreatedWhen = time.Now()
	if r.CreatedBy == uuid.Nil {
		r.CreatedBy = aId
	}
}
