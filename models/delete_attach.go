package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"time"
)

type DeletedAttach struct {
	Id          uuid.UUID `json:"id" gorm:"column:id"`
	ParentId    uuid.UUID `json:"parent_id" gorm:"column:parent_id"`
	Url         string    `json:"url"  gorm:"column:url"`
	Path        string    `json:"-"  gorm:"column:path"`
	OrigName    string    `json:"orig_name" gorm:"column:orig_name"`
	Name        string    `json:"name" gorm:"column:name"`
	CreatedBy   uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen time.Time `json:"created_when" gorm:"column:created_when"`
	Updated     bool      `json:"updated" gorm:"column:updated"`
	UpdatedBy   uuid.UUID `json:"updated_by,omitempty" gorm:"column:updated_by"`
	UpdatedWhen time.Time `json:"updated_when,omitempty" gorm:"column:updated_when"`

	Deleted     bool      `json:"deleted" gorm:"column:deleted"`
	DeletedBy   uuid.UUID `json:"deleted_by" gorm:"column:deleted_by"`
	DeletedWhen time.Time `json:"deleted_when" gorm:"column:deleted_when"`
}

func NewDeleteAttach() *DeletedAttach {
	return &DeletedAttach{}
}

func (a *DeletedAttach) TableName() string {
	return "files"
}

func (a *DeletedAttach) Verify(db *gorm.DB, id string) error {
	return nil
}

func (a *DeletedAttach) Prepare(accId uuid.UUID) {

}

func (a *DeletedAttach) Create(db *gorm.DB) error {
	return nil
}

func (a *DeletedAttach) Update(db *gorm.DB, id uuid.UUID) error {
	return nil
}

func (a *DeletedAttach) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&DeletedAttach{}).Where("id = ?", a.Id).Take(&DeletedAttach{}).Delete(&DeletedAttach{}).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("Post type not found")
		}
		return err
	}
	return nil
}

func (a *DeletedAttach) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&DeletedAttach{}).Where("deleted = true and id = ?", id).Take(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *DeletedAttach) GetByParentId(db *gorm.DB, id uuid.UUID) ([]DeletedAttach, error) {
	all := make([]DeletedAttach, 0)
	err := db.Debug().Model(&DeletedAttach{}).Where("deleted = true and parent_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (a *DeletedAttach) GetAll(db *gorm.DB) (interface{}, error) {
	all := make([]DeletedAttach, 0)
	err := db.Debug().Model(&DeletedAttach{}).Where("deleted = true").Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}
