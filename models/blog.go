package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"html"
	"strings"
	"time"
)

type Blog struct {
	Id          uuid.UUID `json:"id" gorm:"column:id"`
	GroupId     uuid.UUID `json:"group_id" gorm:"column:group_id"`
	Name        string    `json:"name" gorm:"column:name"`
	CreatedBy   uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen time.Time `json:"created_when" gorm:"column:created_when"`
	Updated     bool      `json:"updated" gorm:"column:updated"`
	UpdatedBy   uuid.UUID `json:"updated_by,omitempty" gorm:"column:updated_by"`
	UpdatedWhen time.Time `json:"updated_when,omitempty" gorm:"column:updated_when"`
	Hidden      bool      `json:"hidden" gorm:"column:hidden"`
	TagIds      string    `json:"-" gorm:"column:tags" swaggertype:"-"`
	Tags        []Tag     `json:"tags" gorm:"-"`
}

func (b *Blog) TableName() string {
	return "blogs"
}

func NewBlog() *Blog {
	return &Blog{}
}

func (b *Blog) Create(db *gorm.DB) error {
	err := db.Debug().Model(&Blog{}).Create(b).Error
	if err != nil {
		return err
	}
	return nil
}

func (b *Blog) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&Blog{}).Where("id = ?", id).Take(b).Error
	if err != nil {
		return err
	}
	return nil
}

func (b *Blog) GetAll(db *gorm.DB) (interface{}, error) {
	all := make([]Blog, 0)
	err := db.Debug().Model(&Blog{}).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (b *Blog) Update(db *gorm.DB, id uuid.UUID) error {
	b.Updated = true
	b.UpdatedWhen = time.Now()
	b.UpdatedBy = id
	err := db.Debug().Model(&Blog{}).Save(b).Error
	if err != nil {
		return err
	}
	return nil
}

func (b *Blog) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&Blog{}).Where("id = ?", b.Id).Take(&Blog{}).Delete(&Blog{}).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("reaction type not found")
		}
		return err
	}
	return nil
}

func (b *Blog) Tag(db *gorm.DB, tags string) error {
	b.TagIds = tags
	err := db.Debug().Model(&Blog{}).Save(b).Error
	if err != nil {
		return err
	}
	return nil
}

func (b *Blog) Verify(db *gorm.DB, id string) error {
	if b.Id == uuid.Nil {
		return errors.New("blogId is empty")
	}
	if b.GroupId == uuid.Nil {
		return errors.New("groupId is empty")
	}
	if b.Name == "" {
		return errors.New("blogName is empty")
	}
	if id != "" && b.Id != uuid.Nil && b.Id.String() != id {
		return errors.New("id in URL is not equal to id in body")
	}

	return nil
}

func (b *Blog) Prepare(aId uuid.UUID) {
	b.Id = uuid.New()
	b.Name = html.EscapeString(strings.TrimSpace(b.Name))
	b.CreatedWhen = time.Now()

	if b.CreatedBy == uuid.Nil {
		b.CreatedBy = aId
	}
}
