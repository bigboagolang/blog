package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"html"
	"strings"
	"time"
)

type AttachType struct {
	Id          uuid.UUID `json:"id" gorm:"column:id"`
	Type        string    `json:"type" gorm:"column:type"`
	CreatedBy   uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen time.Time `json:"created_when" gorm:"column:created_when"`
	Updated     bool      `json:"updated" gorm:"column:updated"`
	UpdatedBy   uuid.UUID `json:"updated_by,omitempty" gorm:"column:updated_by"`
	UpdatedWhen time.Time `json:"updated_when,omitempty" gorm:"column:updated_when"`
}

func (a *AttachType) TableName() string {
	return "file_types"
}

func (a *AttachType) Verify(db *gorm.DB, id string) error {
	if a.Id == uuid.Nil {
		return errors.New("id is empty")
	}
	if a.Type == "" {
		return errors.New("file type is empty")
	}
	return nil
}

func (a *AttachType) Prepare(accId uuid.UUID) {
	a.Id = uuid.New()
	a.Type = html.EscapeString(strings.TrimSpace(a.Type))
	a.CreatedWhen = time.Now()

	if a.CreatedBy == uuid.Nil {
		a.CreatedBy = accId
	}
}

func (a *AttachType) Create(db *gorm.DB) error {
	err := db.Debug().Model(&AttachType{}).Create(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *AttachType) GetAll(db *gorm.DB) (interface{}, error) {
	all := make([]AttachType, 5)
	err := db.Debug().Model(&AttachType{}).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (a *AttachType) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&AttachType{}).Where("id = ?", id).Take(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *AttachType) GetByType(db *gorm.DB, t string) error {
	err := db.Debug().Model(&AttachType{}).Where("type = ?", t).Take(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *AttachType) Update(db *gorm.DB, id uuid.UUID) error {
	a.Updated = true
	a.UpdatedWhen = time.Now()
	a.UpdatedBy = id
	err := db.Debug().Model(&AttachType{}).Save(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *AttachType) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&AttachType{}).Where("id = ?", a.Id).Take(&AttachType{}).Delete(&AttachType{}).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("file type not found")
		}
		return err
	}
	return nil
}
