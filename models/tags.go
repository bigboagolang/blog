package models

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type Tag struct {
	Id       uuid.UUID `json:"id" gorm:"column:tag_id"`
	Value    string    `json:"value" gorm:"-"`
	ParentId uuid.UUID `json:"-" gorm:"column:parent_id"`
}

func (t *Tag) TableName() string {
	return "tags"
}

func (t *Tag) Create(db *gorm.DB) error {
	err := db.Debug().Model(&Tag{}).Create(t).Error
	if err != nil {
		return err
	}
	return nil
}

func (t *Tag) GetById(db *gorm.DB, id uuid.UUID) (interface{}, error) {
	all := make([]Tag, 5)
	err := db.Debug().Model(&Tag{}).Where("tag_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}
