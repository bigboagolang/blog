package models

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"html"
	"strings"
	"time"
)

type Post struct {
	Id           uuid.UUID  `json:"id" gorm:"column:id"`
	BlogId       uuid.UUID  `json:"blog_id" gorm:"column:blog_id"`
	Title        string     `json:"title" gorm:"column:title"`
	Content      string     `json:"content" gorm:"column:content"`
	CreatedBy    uuid.UUID  `json:"created_by" gorm:"column:created_by"`
	CreatedWhen  time.Time  `json:"created_when" gorm:"column:created_when"`
	Updated      bool       `json:"updated" gorm:"column:updated"`
	UpdatedBy    uuid.UUID  `json:"updated_by,omitempty" gorm:"column:updated_by"`
	UpdatedWhen  time.Time  `json:"updated_when,omitempty" gorm:"column:updated_when"`
	ToRemove     bool       `json:"to_remove" gorm:"column:to_remove"`
	ToRemoveBy   uuid.UUID  `json:"remove_by,omitempty" gorm:"column:remove_by"`
	ToRemoveWhen time.Time  `json:"remove_when,omitempty" gorm:"column:remove_when"`
	Reactions    []Reaction `json:"reactions,omitempty" gorm:"-"`
	TagIds       string     `json:"-" gorm:"column:tags" swaggertype:"-"`
	Tags         []Tag      `json:"tags" gorm:"-"`
}

func (p *Post) TableName() string {
	return "posts"
}

func NewPost() *Post {
	return &Post{}
}

func (p *Post) Create(db *gorm.DB) error {
	err := db.Debug().Model(&Post{}).Create(p).Error
	if err != nil {
		return err
	}
	return nil
}

func (p *Post) GetAll(db *gorm.DB) (interface{}, error) {
	return nil, nil
}

func (p *Post) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&Post{}).Where("id = ?", id).Take(p).Error
	if err != nil {
		return err
	}
	r, err := GetPostReactionByPostId(db, id)
	if err != nil {
		return err
	}
	p.Reactions = r
	return nil
}

func (p *Post) GetByBlogId(db *gorm.DB, id uuid.UUID) ([]Post, error) {
	all := make([]Post, 0)
	err := db.Debug().Model(&Post{}).Where("blog_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (p *Post) GetByUserId(db *gorm.DB, id uuid.UUID) ([]Post, error) {
	all := make([]Post, 0)
	err := db.Debug().Model(&Post{}).Where("created_by = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

func (p *Post) Tag(db *gorm.DB, tags string) error {
	p.TagIds = tags
	err := db.Debug().Model(&Post{}).Save(p).Error
	if err != nil {
		return err
	}
	return nil
}

func (p *Post) Update(db *gorm.DB, id uuid.UUID) error {
	p.Updated = true
	p.UpdatedWhen = time.Now()
	p.UpdatedBy = id
	err := db.Debug().Model(&Post{}).Save(p).Error
	if err != nil {
		return err
	}
	return nil
}

func (p *Post) Delete(db *gorm.DB) error {
	err := db.Debug().Model(&Post{}).Where("id = ?", p.Id).Take(&Post{}).Delete(&Post{}).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("Post type not found")
		}
		return err
	}
	return nil
}

func (p *Post) Verify(db *gorm.DB, id string) error {
	if p.Id == uuid.Nil {
		return errors.New("id is empty")
	}
	if p.BlogId == uuid.Nil {
		return errors.New("blog_id is empty")
	} else {
		m := Blog{
			Id: uuid.Nil,
		}
		err := m.GetById(db, p.BlogId)
		if err != nil || p.BlogId == uuid.Nil {
			return errors.New("blog_id not found")
		}
	}
	if p.Title == "" {
		return errors.New("title is empty")
	}
	if id != "" && p.Id != uuid.Nil && p.Id.String() != id {
		return errors.New("id in URL is not equal to id in body")
	}
	return nil
}

func (p *Post) Prepare(aId uuid.UUID) {
	p.Id = uuid.New()
	p.CreatedWhen = time.Now()
	p.Title = html.EscapeString(strings.TrimSpace(p.Title))
	p.Content = html.EscapeString(strings.TrimSpace(p.Content))

	if p.CreatedBy == uuid.Nil {
		p.CreatedBy = aId
	}
	p.Updated = false
	p.ToRemove = false
}

func (p *Post) Print() {
	fmt.Println("ID: ", p.Id)

}

func GetPostByAuth(db *gorm.DB, id uuid.UUID) ([]Post, error) {
	var p []Post
	err := db.Debug().Model(&Post{}).Where("created_by = ?", id).Find(&p).Error
	if err != nil {
		return nil, err
	}

	return p, nil
}

func GetPostReactionByPostId(db *gorm.DB, id uuid.UUID) ([]Reaction, error) {
	var rAll []Reaction
	err := db.Debug().Model(&Reaction{}).Where("parent_id = ?", id).Find(&rAll).Error
	if err != nil {
		return nil, err
	}

	return rAll, nil
}
