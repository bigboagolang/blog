package models

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"html"
	"strings"
	"time"
)

type Attach struct {
	Id          uuid.UUID `json:"id" gorm:"column:id"`
	ParentId    uuid.UUID `json:"parent_id" gorm:"column:parent_id"`
	Url         string    `json:"url"  gorm:"column:url"`
	Path        string    `json:"-"  gorm:"column:path"`
	OrigName    string    `json:"orig_name" gorm:"column:orig_name"`
	Name        string    `json:"name" gorm:"column:name"`
	CreatedBy   uuid.UUID `json:"created_by" gorm:"column:created_by"`
	CreatedWhen time.Time `json:"created_when" gorm:"column:created_when"`
	Updated     bool      `json:"updated" gorm:"column:updated"`
	UpdatedBy   uuid.UUID `json:"updated_by,omitempty" gorm:"column:updated_by"`
	UpdatedWhen time.Time `json:"updated_when,omitempty" gorm:"column:updated_when"`

	Deleted     bool      `json:"-" gorm:"column:deleted" swaggertype:"-"`
	DeletedBy   uuid.UUID `json:"-" gorm:"column:deleted_by" swaggertype:"-"`
	DeletedWhen time.Time `json:"-" gorm:"column:deleted_when" swaggertype:"-"`
}

func (a *Attach) TableName() string {
	return "files"
}

func NewAttach() *Attach {
	return &Attach{}
}

func (a *Attach) Verify(db *gorm.DB, id string) error {
	if a.Id == uuid.Nil {
		return errors.New("id is empty")
	}
	if a.ParentId == uuid.Nil {
		return errors.New("parent_id is empty")
	}
	if a.Name == "" {
		return errors.New("name is empty")
	}
	if a.OrigName == "" {
		return errors.New("orig_name is empty")
	}
	if id != "" && a.Id != uuid.Nil && a.Id.String() != id {
		return errors.New("id in URL is not equal to id in body")
	}

	return nil
}

func (a *Attach) Prepare(accId uuid.UUID) {
	a.Id = uuid.New()
	a.OrigName = html.EscapeString(strings.TrimSpace(a.OrigName))
	a.Name = html.EscapeString(strings.TrimSpace(a.Name))
	a.Path = html.EscapeString(strings.TrimSpace(a.Path))
	a.CreatedWhen = time.Now()
	a.Deleted = false

	if a.CreatedBy == uuid.Nil {
		a.CreatedBy = accId
	}
}

func (a *Attach) Create(db *gorm.DB) error {
	err := db.Debug().Model(&Attach{}).Create(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *Attach) GetById(db *gorm.DB, id uuid.UUID) error {
	err := db.Debug().Model(&Attach{}).Where("deleted = false and id = ?", id).Take(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *Attach) GetByParentId(db *gorm.DB, id uuid.UUID) ([]Attach, error) {
	all := make([]Attach, 0)
	err := db.Debug().Model(&Attach{}).Where("deleted = false and parent_id = ?", id).Find(&all).Error
	if err != nil {
		return nil, err
	}
	return all, nil
}

//Too big result
func (a *Attach) GetAll(db *gorm.DB) (interface{}, error) {
	return nil, nil
}

func (a *Attach) Update(db *gorm.DB, id uuid.UUID) error {
	a.Updated = true
	a.UpdatedWhen = time.Now()
	a.UpdatedBy = id
	err := db.Debug().Model(&Attach{}).Save(a).Error
	if err != nil {
		return err
	}
	return nil
}

func (a *Attach) Delete(db *gorm.DB) error {
	//err := db.Debug().Model(&Attach{}).Where("id = ?", a.Id).Take(&Attach{}).Delete(&Attach{}).Error

	a.Deleted = true
	a.DeletedWhen = time.Now()
	err := db.Debug().Model(&Attach{}).Save(a).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return errors.New("attach not found")
		}
		return err
	}
	return nil
}
