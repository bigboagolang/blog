package sql

const (
	GetById                          = "select * from reactions where id = ?"
	GetAllPostsAttachByBlogId        = "select f.* from files f, posts p where f.parent_id = p.id and p.blog_id = ?"
	GetAllPostCommentsAttachByBlogId = "select f.* from files f, posts p, post_comments c where f.parent_id = c.id and c.post_id = p.id and p.blog_id = ?"
	GetAllPostCommentsAttachByPostId = "select f.* from files f, post_comments c where f.parent_id = c.id and c.post_id = ?"
	GetAllAttachByParentId           = "select f.* from files f where f.parent_id = ?"
)
