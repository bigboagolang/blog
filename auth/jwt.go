package auth

import (
	"context"
	"github.com/coreos/go-oidc"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"gitlab.com/bigboagolang/blog/models"
	"gitlab.com/bigboagolang/blog/responses"
	"log"
	"net/http"
	"strings"
)

const (
	RoleAdmin = "ROLE_ADMIN"
)

type RealmAccess struct {
	Roles []string `json:"roles"`
}

type AccessToken struct {
	//AccountId string      `json:"account_id"`
	AccountId uuid.UUID   `json:"account_id"`
	ResAcc    RealmAccess `json:"realm_access"`
}

var (
	clientID   string
	configURL  string
	provider   *oidc.Provider
	ctx        context.Context
	oidcConfig *oidc.Config
	verifier   *oidc.IDTokenVerifier

	err error
)

func InitAuthConf(id, url string) {
	clientID = id
	configURL = url
	ctx = context.Background()
	provider, err = oidc.NewProvider(ctx, configURL)
	if err != nil {
		log.Fatal(err)
	}
	oidcConfig = &oidc.Config{
		ClientID: clientID,
	}
	verifier = provider.Verifier(oidcConfig)
}

func checkToken(w http.ResponseWriter, r *http.Request) *oidc.IDToken {
	rawAccessToken := r.Header.Get("Authorization")
	if rawAccessToken == "" {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return nil
	}
	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {
		responses.ERROR(w, http.StatusUnauthorized, err)
		return nil
	}

	token, err := verifier.Verify(ctx, parts[1])
	if err != nil {
		responses.ERROR(w, http.StatusForbidden, err)
		return nil
	}
	return token
}

func GetAccessToken(w http.ResponseWriter, r *http.Request) *AccessToken {
	a := &AccessToken{}
	token := checkToken(w, r)
	if token == nil {
		return nil
	}

	err := token.Claims(a)
	if err != nil {
		responses.ERROR(w, http.StatusForbidden, err)
		return nil
	}

	return a
}

func CheckTokenRole(role string, w http.ResponseWriter, r *http.Request) bool {
	a := GetAccessToken(w, r)
	if a == nil {
		return false
	}
	for _, v := range a.ResAcc.Roles {
		if v == role {
			return true
		}
	}
	return false
}

func checkUserRole(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	m := models.BlogAdmin{}
	u, err := m.GetByUserAndBlogId(db, uId, oId)
	if err != nil {
		log.Println("CheckUserRole: ", err)
		return false
	}
	if len(u) > 0 {
		return true
	}
	return false
}

func checkBlogOwnerRole(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	blog := models.NewBlog()
	err := blog.GetById(db, oId)
	if err != nil {
		log.Println("checkBlogOwnerRole: ", err)
		return false
	}

	return blog.CreatedBy == uId
}

func checkPostOwnerRole(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	blog := models.NewPost()
	err := blog.GetById(db, oId)
	if err != nil {
		log.Println("checkPostOwnerRole: ", err)
		return false
	}
	return blog.CreatedBy == uId
}

func checkPostCommentOwnerRole(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	blog := models.NewPostComment()
	err := blog.GetById(db, oId)
	if err != nil {
		log.Println("checkPostCommentOwnerRole: ", err)
		return false
	}
	return blog.CreatedBy == uId
}

func CanDeleteBlog(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	return checkBlogOwnerRole(db, oId, uId)
}

func CanUpdateBlog(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	return checkBlogOwnerRole(db, oId, uId) || checkUserRole(db, oId, uId)
}

func CanUpdatePost(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	post := models.NewPost()
	err := post.GetById(db, oId)
	if err != nil {
		log.Println("CanUpdatePost: ", err)
		return false
	}
	return checkPostOwnerRole(db, oId, uId) || checkUserRole(db, post.BlogId, uId)
}

func CanDeletePost(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) (bool, bool) {
	post := models.NewPost()
	err := post.GetById(db, oId)
	if err != nil {
		log.Println("CanUpdatePost: ", err)
		return false, false
	}
	return checkPostOwnerRole(db, oId, uId), checkUserRole(db, post.BlogId, uId)
}

func CanUpdatePostComment(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) bool {
	pComm := models.NewPostComment()
	err := pComm.GetById(db, oId)
	if err != nil {
		log.Println("CanUpdatePostComment: ", err)
		return false
	}
	post := models.NewPost()
	err = post.GetById(db, pComm.PostId)
	if err != nil {
		log.Println("CanUpdatePostComment: ", err)
		return false
	}
	return checkPostOwnerRole(db, pComm.PostId, uId) || checkPostCommentOwnerRole(db, oId, uId) || checkUserRole(db, post.BlogId, uId)
}

func CanDeletePostComment(db *gorm.DB, oId uuid.UUID, uId uuid.UUID) (bool, bool) {
	pComm := models.NewPostComment()
	err := pComm.GetById(db, oId)
	if err != nil {
		log.Println("CanDeletePostComment: ", err)
		return false, false
	}
	post := models.NewPost()
	err = post.GetById(db, pComm.PostId)
	if err != nil {
		log.Println("CanUpdatePostComment: ", err)
		return false, false
	}
	return checkPostOwnerRole(db, pComm.PostId, uId) || checkPostCommentOwnerRole(db, oId, uId), checkUserRole(db, post.BlogId, uId)
}
